#! /bin/sh
#
# compile_java_code.sh
# Copyright (C) 2019 Uwe Schitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

set -x
set -e

JDK_HOME=/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home

JAVAC=${JDK_HOME}/bin/javac
JAR=${JDK_HOME}/bin/jar

MZMINE_JARS=$(cat ~/.emzed3/emzed.ext.mzmine2/mzmine2/mzmine2_home)/lib
echo ${MZMINE_JARS}
cd src/emzed_ext_mzmine2/java

BUILD_ROOT=$(mktemp -d)/ch/ethz/id/sis/emzed

mkdir -p ${BUILD_ROOT}
cp *.java ${BUILD_ROOT}

${JAVAC} -Xlint:unchecked -cp "${MZMINE_JARS}/*" ${BUILD_ROOT}/*.java
echo
${JAR} cf extensions.jar ${BUILD_ROOT}/../../../../../ch
set +x

echo
echo
echo DONE
