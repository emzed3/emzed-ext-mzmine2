#! /usr/bin/env python
import glob
import json
import os
import sys
import zipfile
from pprint import pprint

import jpype

from emzed.ext.mzmine2._installers import get_jre_home, get_mzmine2_home

mzmine2_home = get_mzmine2_home()

here = os.path.join("src", "emzed_ext_mzmine2")
jars = glob.glob(os.path.join(get_mzmine2_home(), "lib", "*.jar"))

class_path = ":".join([*jars, os.path.join(here, "java", "extensions.jar")])

assert sys.platform == "darwin", "must adapt the next line for other platforms"

jvmpath = os.path.join(
    get_jre_home(), "Contents", "Home", "lib", "server", "libjvm.dylib"
)

jpype.startJVM(
    jvmpath, "-Djava.awt.headless=true", convertStrings=True, classpath=class_path
)


clz = jpype.JClass("net.sf.mzmine.main.MZmineCore").class_
field = clz.getDeclaredField("projectManager")

field.setAccessible(True)
impl = jpype.JClass("net.sf.mzmine.project.impl.ProjectManagerImpl")()
impl.initModule()
field.set(None, impl)

field = clz.getDeclaredField("configuration")
field.setAccessible(True)
field.set(None, jpype.JClass("net.sf.mzmine.main.impl.MZmineConfigurationImpl")())

field = clz.getDeclaredField("desktop")
field.setAccessible(True)
field.set(None, jpype.JClass("net.sf.mzmine.desktop.impl.HeadLessDesktop")())

with open("mzmine2_classes.json", "r") as fh:
    entries = json.load(fh)


Parameter = jpype.JClass(entries["Parameter"])
Modifier = jpype.JClass("java.lang.reflect.Modifier")


def is_abstract_class(jcls):
    return Modifier.isAbstract(jcls.class_.getModifiers())


args = jpype.JArray(Parameter, 1)(0)


def _choice_as_str(choice):
    try:
        return choice.name()
    except AttributeError:
        return str(choice)


"""

      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

      Document configuration = dBuilder.newDocument();
      Element configRoot = configuration.createElement("configuration");
      configuration.appendChild(configRoot);

      Element prefElement = configuration.createElement("preferences");
      configRoot.appendChild(prefElement);
      preferences.setSkipSensitiveParameters(skipSensitive);
      preferences.saveValuesToXML(prefElement);

      Element lastFilesElement = configuration.createElement("lastprojects");
      configRoot.appendChild(lastFilesElement);
      lastProjects.saveValueToXML(lastFilesElement);

      """


def main():

    result = {}

    for class_name, class_path in entries.items():

        if class_name == "AdductType":
            jcls = jpype.JClass(class_path)
            defaults = jcls.getDefaultValues()
            data = [
                (adduct.getName(), adduct.getMassDifference()) for adduct in defaults
            ]
            with open("src/emzed_ext_mzmine2/_mzmine2_default_adducts.json", "w") as fh:
                json.dump(data, fh, indent=4)
            continue

        if not class_name.endswith("Parameters"):
            continue

        # print(class_path)

        jcls = jpype.JClass(class_path)
        if is_abstract_class(jcls):
            continue
        jobj = jcls()
        fields = {}

        for field_name in jobj.__dir__():
            field = getattr(jobj, field_name, None)
            if not isinstance(field, Parameter):
                continue
            if not hasattr(field, "getDescription"):
                continue
            java_path = field.__class__.__name__
            field_type = java_path.rsplit(".", 1)[1]
            default = field.getValue()
            if default.__class__.__qualname__ == "java.lang.Boolean":
                default = bool(int(default))
            choices = None
            value_range = None
            if "Combo" in field_type and hasattr(field, "getChoices"):
                choices = [_choice_as_str(c) for c in field.getChoices()]
                if default is None and choices:
                    default = choices[0]
                elif default:
                    default = _choice_as_str(default)
            elif "PercentParameter" in field_type:
                field_descriptor = field.__class__.class_.getDeclaredField("minValue")
                field_descriptor.setAccessible(True)
                min_value = field_descriptor.get(field)
                field_descriptor = field.__class__.class_.getDeclaredField("maxValue")
                field_descriptor.setAccessible(True)
                max_value = field_descriptor.get(field)
                value_range = [min_value, max_value]

            elif "MZToleranceParameter" in field_type:
                if default is not None:
                    default = [default.getMzTolerance(), default.getPpmTolerance()]
            elif field_type == "DoubleRangeParameter":
                if default is not None:
                    default = [default.lowerEndpoint(), default.upperEndpoint()]
            try:
                json.dumps(default)
            except TypeError:
                print(
                    "can not handle default value",
                    default,
                    "for",
                    field_name,
                    "of type",
                    field_type,
                    "in",
                    class_name,
                )
                default = None

            entry = dict(
                type=field_type,
                java_path=java_path,
                name=field.getName(),
                description=field.getDescription(),
                choices=choices,
                default=default,
                value_range=value_range,
            )
            try:
                json.dumps(entry)
            except TypeError as e:
                print(e)
                breakpoint()
                pass
            fields[field_name] = entry

        result[class_name] = dict(class_path=class_path, fields=fields)

    # just check if all data can be serialized before we write. This avoids a corrupted
    # result file as an exampetion during writing leaves an incomplete json file
    # behind:
    json.dumps(result)

    with open("src/emzed_ext_mzmine2/_mzmine2_parameter_classes.json", "w") as fh:
        json.dump(result, fh, indent=4)


main()
