#! /bin/sh
#
# upload_example_scripts.sh
# Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

BASE_FOLDER=$(mktemp -d)
FOLDER=${BASE_FOLDER}/emzed_ext_mzmine2
mkdir ${FOLDER}
cp -R example_scripts/* ${FOLDER}
pushd ${BASE_FOLDER}
zip -r example_scripts_emzed_ext_mzmine2.zip emzed_ext_mzmine2
scp example_scripts_emzed_ext_mzmine2.zip emzed:htdocs/downloads/
popd
