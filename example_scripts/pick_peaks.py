#! /usr/bin/env python
# Copyright 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>

import emzed
import emzed.ext.mzmine2 as mzm

peakmap = emzed.io.load_peak_map("test.mzXML")

chromatogram_builder = mzm.ADAPChromatogramBuilder(
    # use 5 ppm, ignore absolute value in Dalton:
    mz_tolerance=(0.0, 5),
    start_intensity=100_000,
    intensity_thresh2=100_000,
    minimum_scan_span=5,
)

print(chromatogram_builder)

peak_detector = mzm.ADAPDetector(
    peak_duration=(2, 10),
    rt_for_cwt_scales_duration=(0.01, 1),
    sn_estimators=mzm.IntensityWindowsSNParameters(),
)

print(peak_detector)

peaks = mzm.pick_peaks(peakmap, chromatogram_builder, peak_detector)

emzed.io.save_table(peaks, "picked_peaks.table", overwrite=True)

emzed.gui.inspect(peaks)
