#! /usr/bin/env python
# Copyright 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>

import emzed
import emzed.ext.mzmine2 as mzm

peakmap = emzed.io.load_peak_map("test.mzXML")

parameters = mzm.RemoveShoulderPeaksParameters()
parameters.resolution = 10_000

# can be ommited, is default setting.
parameters.peak_model = "GAUSS"

print(parameters)

cleaned_peakmap = mzm.remove_shoulder_peaks(peakmap, parameters)

# overwrite is needed when you run the script a second time:
emzed.io.save_peak_map(cleaned_peakmap, "test_cleaned.mzXML", overwrite=True)

# this shows an overlay of two peakmaps. zoom in around a large peak
# the removed peaks will be shown in blue:
emzed.gui.inspect(peakmap, cleaned_peakmap)
