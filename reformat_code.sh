#!/bin/sh
#
# reformat_code.sh
# Copyright (C) 2019 Uwe Schitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

isort src tests benchmarks
black src tests benchmarks

java -jar development_tools/google-java-format-1.7-all-deps.jar --replace src/emzed_ext_mzmine2/java/*.java
astyle -n -A2 -p -xg -H -xC88 src/emzed_ext_mzmine2/java/*.java
