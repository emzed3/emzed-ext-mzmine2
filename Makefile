.PHONY: help clean clean-pyc clean-build list test test-all coverage docs release sdist

help:
	@echo
	@echo "check       - check code with flake8"
	@echo "style-check - run tests quickly with the default Python"
	@echo "test-all    - run tests on every Python version with tox"
	@echo "coverage    - check code coverage quickly with the default Python"
	@echo "docs        - generate Sphinx HTML documentation, including API docs"
	@echo "sdist       - package"

clean: clean-build clean-pyc

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -rf {} +

style-check:
	flake8 --max-line-length=88 --ignore E203,W503 tests src/emzed_ext_mzmine2

test:
	py.test tests

test-all:
	tox -r

coverage:
	coverage run --source emzed.ext.mzmine2 -m pytest tests
	coverage report -m
	coverage html
	open htmlcov/index.html


build_docs:
	sphinx-apidoc --implicit-namespaces -e -o docs -f src/emzed_ext_mzmine2/
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	cp -R htmlcov docs/_build/html


update_docs:
	$(MAKE) -C docs clean
	$(MAKE) -C docs html
	cp -R htmlcov docs/_build/html

deploy_docs: update_docs
	scp -r docs/_build/html/* w3_emzed2@emzed.ethz.ch:htdocs/3/emzed.ext.mzmine2

docs: build_docs
	open docs/_build/html/index.html

sdist: clean
	python setup.py sdist
	ls -l dist
