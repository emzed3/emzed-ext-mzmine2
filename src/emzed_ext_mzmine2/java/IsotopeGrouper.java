/*
 * AdductSearch.java
 * Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
 *
 * Distributed under terms of the MIT license.
 */

package ch.ethz.id.sis.emzed;

import java.io.*;
import java.lang.reflect.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import net.sf.mzmine.datamodel.DataPoint;
import net.sf.mzmine.datamodel.Feature;
import net.sf.mzmine.datamodel.IsotopePattern;
import net.sf.mzmine.datamodel.MZmineProject;
import net.sf.mzmine.datamodel.PeakList;
import net.sf.mzmine.datamodel.PeakListRow;
import net.sf.mzmine.datamodel.RawDataFile;
import net.sf.mzmine.datamodel.impl.SimplePeakList;
import net.sf.mzmine.modules.peaklistmethods.isotopes.deisotoper.*;
import net.sf.mzmine.parameters.ParameterSet;
import net.sf.mzmine.parameters.parametertypes.tolerances.MZTolerance;
import net.sf.mzmine.parameters.parametertypes.tolerances.RTTolerance;
import net.sf.mzmine.project.impl.MZmineProjectImpl;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.json.*;

public class IsotopeGrouper {

    private static PeakList readPeakList(String path, RawDataFile pm)
    throws IOException, IllegalAccessException, NoSuchFieldException {

        Float rt, rtmin, rtmax, height;
        Double mz, mzmin, mzmax;
        Integer id;

        System.out.println(path);

        RawDataFile[] df = new RawDataFile[] {pm};
        PeakList pl = new SimplePeakList("", df);

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            if ((line = br.readLine()) != null)
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(" ");
                    id = Integer.parseInt(values[0]);
                    rt = Float.parseFloat(values[1]);
                    mz = Double.parseDouble(values[2]);
                    height = Float.parseFloat(values[3]);
                    EmzedPeakListRow row = new EmzedPeakListRow(id, rt, 0, 0, mz, 0, 0, height, pm);
                    pl.addRow(row);
                }
        }

        return pl;
    }

    private static void _main(String[] args) throws Exception {

        new ParentShutdownChecker().start();
        WrapperBase.setupMzMine2Core();

        if (args.length != 4)
            throw new IllegalArgumentException(
                "need 4 parameters: in_file, out_path,  config_path and peakmap_path");

        String inPath = args[0];
        String outPath = args[1];
        String config_path = args[2];
        String peakmapPath = args[3];

        System.out.println("json file\n" + config_path);

        String jsonString = new String(Files.readAllBytes(Paths.get(config_path)));

        JSONObject json = new JSONObject(jsonString);

        JSONArray rtToleranceArray = json.getJSONArray("rt_tolerance");
        Boolean rtToleranceIsAbsolute = rtToleranceArray.getBoolean(0);
        Double rtToleranceValue = rtToleranceArray.getDouble(1);

        JSONArray mzToleranceArray = json.getJSONArray("mz_tolerance");
        Double mzTolerancerAbs = mzToleranceArray.getDouble(0);
        Double mzTolerancerPpm = mzToleranceArray.getDouble(1);

        Boolean monotonicShape = json.getBoolean("monotonic_shape");
        Integer maximumCharge = json.getInt("maximum_charge");
        String representativeIsotope = json.getString("representative_isotope");

        RawDataFile pm = WrapperBase.readInputFile(peakmapPath);

        IsotopeGrouperParameters p = new IsotopeGrouperParameters();

        p.rtTolerance.setValue(new RTTolerance(rtToleranceIsAbsolute, rtToleranceValue));
        p.mzTolerance.setValue(new MZTolerance(mzTolerancerAbs, mzTolerancerPpm));
        p.monotonicShape.setValue(monotonicShape);
        p.maximumCharge.setValue(maximumCharge);
        p.representativeIsotope.setValue(representativeIsotope);
        p.suffix.setValue("");
        p.autoRemove.setValue(false);

        PeakList pl = readPeakList(inPath, pm);

        Feature peak;

        HashMap<ImmutablePair<Double, Double>, Integer> map =
            new HashMap<ImmutablePair<Double, Double>, Integer>();
        for (PeakListRow row : pl.getRows()) {
            peak = row.getPeaks()[0];
            map.put(new ImmutablePair<Double, Double>(peak.getMZ(), peak.getHeight()), row.getID());
        }

        MZmineProjectImpl project = new MZmineProjectImpl();

        // IsotopeGrouperTask is not public, lets fix this!
        Class<?> clazz =
            Class.forName(
                "net.sf.mzmine.modules.peaklistmethods.isotopes.deisotoper.IsotopeGrouperTask");
        Class[] cArg = new Class[3];
        cArg[0] = MZmineProject.class;
        cArg[1] = PeakList.class;
        cArg[2] = ParameterSet.class;
        Constructor<?> constructor = clazz.getDeclaredConstructor(cArg);
        constructor.setAccessible(true);

        Object task = constructor.newInstance(project, pl, p);
        Method method = task.getClass().getMethod("run");
        method.setAccessible(true);
        method.invoke(task);

        PrintWriter writer = new PrintWriter(outPath, "UTF-8");
        System.out.println("write output file to " + outPath);

        PeakList pl_deconv = project.getPeakLists()[0];
        System.out.println();
        System.out.println("extracted " + pl.getNumberOfRows() + " deconvolved peaks");
        System.out.println("extracted " + pl_deconv.getNumberOfRows() + " deconvolved peaks");
        System.out.println();

        IsotopePattern pattern;
        Integer other_id;
        Integer isotope_group_id = 0;
        try {
            for (PeakListRow row : pl_deconv.getRows()) {
                peak = row.getPeaks()[0];
                pattern = peak.getIsotopePattern();
                if (pattern != null)
                    for (DataPoint dp : pattern.getDataPoints()) {
                        other_id = map.get(new ImmutablePair<Double, Double>(dp.getMZ(), dp.getIntensity()));
                        writer.println(
                            isotope_group_id + " " + row.getID() + " " + other_id + " " + peak.getCharge());
                    }
                isotope_group_id += 1;
            }
        } finally {
            writer.close();
        }
    }

    public static void main(String[] args) {
        try {
            _main(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("!!!ERROR");
            return;
        }
        System.out.println("!!!DONE");
        System.exit(0);
    }
}
