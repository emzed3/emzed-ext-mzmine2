/*
 * AdductSearch.java
 * Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
 *
 * Distributed under terms of the MIT license.
 */

package ch.ethz.id.sis.emzed;

import com.google.common.collect.Range;
import java.io.*;
import java.lang.reflect.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import net.sf.mzmine.datamodel.DataPoint;
import net.sf.mzmine.datamodel.Feature;
import net.sf.mzmine.datamodel.IsotopePattern;
import net.sf.mzmine.datamodel.MZmineProject;
import net.sf.mzmine.datamodel.PeakList;
import net.sf.mzmine.datamodel.PeakListRow;
import net.sf.mzmine.datamodel.RawDataFile;
import net.sf.mzmine.datamodel.impl.SimpleDataPoint;
import net.sf.mzmine.datamodel.impl.SimpleFeature;
import net.sf.mzmine.datamodel.impl.SimpleIsotopePattern;
import net.sf.mzmine.datamodel.impl.SimplePeakList;
import net.sf.mzmine.datamodel.impl.SimplePeakListRow;
import net.sf.mzmine.modules.peaklistmethods.alignment.join.*;
import net.sf.mzmine.modules.peaklistmethods.isotopes.isotopepatternscore.*;
import net.sf.mzmine.parameters.ParameterSet;
import net.sf.mzmine.parameters.parametertypes.selectors.PeakListsSelection;
import net.sf.mzmine.parameters.parametertypes.selectors.PeakListsSelectionType;
import net.sf.mzmine.parameters.parametertypes.tolerances.MZTolerance;
import net.sf.mzmine.parameters.parametertypes.tolerances.RTTolerance;
import net.sf.mzmine.project.impl.MZmineProjectImpl;
import net.sf.mzmine.project.impl.RawDataFileImpl;
import org.json.*;

public class JoinAligner {

    private static PeakList[] readPeakLists(
        String path,
        HashMap<Integer, IsotopePattern> patterns,
        Integer maxPeakId,
        Boolean readCharge,
        Integer numberOfPeakLists)
    throws IOException, IllegalAccessException, NoSuchFieldException {

        Float rt, rtmin, rtmax, height;
        Double mz, mzmin, mzmax;
        Integer id, charge, peakTableId;

        PeakList[] peakLists = new SimplePeakList[numberOfPeakLists];
        RawDataFile[] rawDataFiles = new RawDataFileImpl[numberOfPeakLists];

        for (int i = 0; i < numberOfPeakLists; i++) {
            rawDataFiles[i] = new RawDataFileImpl("" + i);
            peakLists[i] = new SimplePeakList("", new RawDataFile[] {rawDataFiles[i]});
        }

        DataPoint[] fakeDataPoints = new SimpleDataPoint[] {new SimpleDataPoint(1.0, 1.0)};

        // f.setIsotopePattern
        charge = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            if ((line = br.readLine()) != null)
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(" ");
                    id = Integer.parseInt(values[0]);
                    rt = Float.parseFloat(values[1]);
                    mz = Double.parseDouble(values[2]);
                    if (readCharge) {
                        charge = Integer.parseInt(values[3]);
                        peakTableId = Integer.parseInt(values[4]);
                    } else {
                        peakTableId = Integer.parseInt(values[3]);
                    }
                    SimpleFeature f =
                        new SimpleFeature(
                        rawDataFiles[peakTableId],
                        mz,
                        rt,
                        0.0,
                        0.0,
                        new int[0],
                        fakeDataPoints,
                        Feature.FeatureStatus.MANUAL,
                        id, // misusing scan number to reidentify peaks later
                        0,
                        new int[0],
                        Range.closed(new Double(rt), new Double(rt)),
                        Range.closed(mz, mz),
                        Range.closed(0.0, 0.0));
                    f.setCharge(charge);
                    Integer globalId = peakTableId * (maxPeakId + 1) + id;
                    if (patterns.containsKey(globalId)) {
                        f.setIsotopePattern(patterns.get(globalId));
                    }
                    SimplePeakListRow row = new SimplePeakListRow(id);
                    row.addPeak(rawDataFiles[peakTableId], f);
                    peakLists[peakTableId].addRow(row);
                }
        }

        return peakLists;
    }

    private static HashMap<Integer, IsotopePattern> readIsotopePatterns(String path)
    throws IOException, JSONException {
        // TODO: IsotopePattern anstatt DataPoints?
        HashMap<Integer, IsotopePattern> result = new HashMap<>();
        String jsonString = new String(Files.readAllBytes(Paths.get(path)));
        JSONObject json = new JSONObject(jsonString);
        String[] names = JSONObject.getNames(json);
        if (names == null) return result;
        for (String key : names) {
            ArrayList<SimpleDataPoint> dataPoints = new ArrayList<>();
            JSONArray entries = json.getJSONArray(key);
            for (int i = 0; i < entries.length(); i++) {
                JSONArray point = entries.getJSONArray(i);
                Double mz = point.getDouble(0);
                Double intensity = point.getDouble(1);
                dataPoints.add(new SimpleDataPoint(mz, intensity));
            }
            Integer peakId = Integer.parseInt(key);
            result.put(
                peakId,
                new SimpleIsotopePattern(
                    dataPoints.toArray(new SimpleDataPoint[0]),
                    IsotopePattern.IsotopePatternStatus.DETECTED,
                    ""));
        }
        return result;
    }

    private static void _main(String[] args) throws Exception {

        new ParentShutdownChecker().start();
        WrapperBase.setupMzMine2Core();

        if (args.length != 6)
            throw new IllegalArgumentException(
                "need 4 parameters: in_file, out_path,  config_path, isotope_patterns_file,"
                + " max_peak_id and number_of_peaklists");

        String peakListPath = args[0];
        String outPath = args[1];
        String configPath = args[2];
        String isotopePatternsPath = args[3];
        Integer maxPeakId = Integer.parseInt(args[4]);
        Integer numberOfPeakLists = Integer.parseInt(args[5]);

        System.out.println("json file\n" + configPath);

        String jsonString = new String(Files.readAllBytes(Paths.get(configPath)));

        JSONObject json = new JSONObject(jsonString);

        JSONArray rtToleranceArray = json.getJSONArray("rt_tolerance");
        Boolean rtToleranceIsAbsolute = rtToleranceArray.getBoolean(0);
        Double rtToleranceValue = rtToleranceArray.getDouble(1);

        JSONArray mzToleranceArray = json.getJSONArray("mz_tolerance");
        Double mzToleranceAbs = mzToleranceArray.getDouble(0);
        Double mzTolerancePpm = mzToleranceArray.getDouble(1);

        Double mzWeight = json.getDouble("mz_weight");
        Double rtWeight = json.getDouble("rt_weight");
        Boolean sameChargeRequired = json.getBoolean("same_charge_required");

        JoinAlignerParameters p = new JoinAlignerParameters();

        p.RTTolerance.setValue(new RTTolerance(rtToleranceIsAbsolute, rtToleranceValue));
        p.MZTolerance.setValue(new MZTolerance(mzToleranceAbs, mzTolerancePpm));
        p.RTWeight.setValue(rtWeight);
        p.MZWeight.setValue(mzWeight);
        p.SameChargeRequired.setValue(sameChargeRequired);
        p.compareIsotopePattern.setValue(false);
        p.peakListName.setValue("aligned");
        p.SameIDRequired.setValue(false);

        if (json.has("isotope_pattern_score_parameters")) {
            JSONObject ipsc = json.getJSONObject("isotope_pattern_score_parameters");
            mzToleranceArray = ipsc.getJSONArray("mz_tolerance");
            Double mzToleranceAbsIsotopes = mzToleranceArray.getDouble(0);
            Double mzTolerancePpmIsotopes = mzToleranceArray.getDouble(1);
            Double isotopeNoiseLevel = ipsc.getDouble("isotope_noise_level");
            Double isotopePatternScoreThreshold = ipsc.getDouble("isotope_pattern_score_threshold");

            ParameterSet params = p.compareIsotopePattern.getEmbeddedParameters();
            params
            .getParameter(IsotopePatternScoreParameters.mzTolerance)
            .setValue(new MZTolerance(mzToleranceAbsIsotopes, mzTolerancePpm));

            params
            .getParameter(IsotopePatternScoreParameters.isotopeNoiseLevel)
            .setValue(isotopeNoiseLevel);
            params
            .getParameter(IsotopePatternScoreParameters.isotopePatternScoreThreshold)
            .setValue(isotopePatternScoreThreshold);
            p.compareIsotopePattern.setValue(true);
        }

        HashMap<Integer, IsotopePattern> patterns = readIsotopePatterns(isotopePatternsPath);
        PeakList[] pl =
            readPeakLists(peakListPath, patterns, maxPeakId, sameChargeRequired, numberOfPeakLists);
        System.out.println(pl);

        PeakListsSelection pls = new PeakListsSelection();
        pls.setSelectionType(PeakListsSelectionType.SPECIFIC_PEAKLISTS);
        pls.setSpecificPeakLists(pl);
        p.peakLists.setValue(pls);

        MZmineProjectImpl project = new MZmineProjectImpl();

        // JoinAlignerTask is not public, lets fix this!
        Class<?> clazz =
            Class.forName("net.sf.mzmine.modules.peaklistmethods.alignment.join.JoinAlignerTask");
        Class[] cArg = new Class[2];
        cArg[0] = MZmineProject.class;
        cArg[1] = ParameterSet.class;
        Constructor<?> constructor = clazz.getDeclaredConstructor(cArg);
        constructor.setAccessible(true);

        Object task = constructor.newInstance(project, p);
        Method method = task.getClass().getMethod("run");
        method.setAccessible(true);
        method.invoke(task);

        PeakList result = project.getPeakLists()[0];
        System.out.println("write output file to " + outPath);

        PrintWriter writer = new PrintWriter(outPath, "UTF-8");

        try {
            for (int i = 0; i < result.getNumberOfRows(); i++) {
                PeakListRow row = result.getRow(i);
                for (Feature f : row.getPeaks()) {
                    writer.print(f.getDataFile().getName() + " ");
                    // we "misused" the scan number to store the original peak id:
                    writer.print(f.getRepresentativeScanNumber() + " ");
                }
                writer.println();
            }
        } finally {
            writer.close();
        }
    }

    public static void main(String[] args) {
        try {
            _main(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("!!!ERROR");
            return;
        }
        System.out.println("!!!DONE");
        System.exit(0);
    }
}
