/*
 * EmzedPeakListRow.java
 * Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
 *
 * Distributed under terms of the MIT license.
 */

package ch.ethz.id.sis.emzed;

import io.github.msdk.datamodel.SimpleChromatogram;
import net.sf.mzmine.datamodel.Feature;
import net.sf.mzmine.datamodel.RawDataFile;
import net.sf.mzmine.datamodel.impl.SimpleFeature;
import net.sf.mzmine.datamodel.impl.SimplePeakListRow;

public class EmzedPeakListRow extends SimplePeakListRow {
    public EmzedPeakListRow(
        int id,
        float rt,
        float rtmin,
        float rtmax,
        double mz,
        double mzmin,
        double mzmax,
        float height,
        RawDataFile pm) {
        super(id);
        addPeak(pm, createFeature(rt, rtmin, rtmax, mz, mzmin, mzmax, height, pm));
    }

    private static Feature createFeature(
        float rt,
        float rtmin,
        float rtmax,
        double mz,
        double mzmin,
        double mzmax,
        float height,
        RawDataFile pm) {
        io.github.msdk.datamodel.SimpleFeature msdkFeature =
            new io.github.msdk.datamodel.SimpleFeature();
        msdkFeature.setMz(mz);
        msdkFeature.setRetentionTime(rt);
        msdkFeature.setHeight(height);
        msdkFeature.setArea(height * (rtmax - rtmin) / 2);
        SimpleChromatogram c = new SimpleChromatogram();
        c.setDataPoints(
            new float[] {rtmin, rtmax}, new double[] {mzmin, mzmax}, new float[] {height, height},
            2);
        msdkFeature.setChromatogram(c);

        return new SimpleFeature(pm, Feature.FeatureStatus.DETECTED, msdkFeature);
    }
}
