/*
 * AdductSearch.java
 * Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
 *
 * Distributed under terms of the MIT license.
 */

package ch.ethz.id.sis.emzed;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import net.sf.mzmine.datamodel.PeakIdentity;
import net.sf.mzmine.datamodel.PeakList;
import net.sf.mzmine.datamodel.PeakListRow;
import net.sf.mzmine.datamodel.RawDataFile;
import net.sf.mzmine.datamodel.impl.SimplePeakList;
import net.sf.mzmine.datamodel.impl.SimplePeakListRow;
import net.sf.mzmine.modules.peaklistmethods.identification.adductsearch.*;
import net.sf.mzmine.parameters.parametertypes.tolerances.MZTolerance;
import net.sf.mzmine.parameters.parametertypes.tolerances.RTTolerance;
import org.json.*;

public class AdductSearch {

    private static PeakList readPeakList(String path)
    throws IOException, IllegalAccessException, NoSuchFieldException {

        Double mz, rt, height;
        Integer id;

        System.out.println(path);

        RawDataFile[] df = new RawDataFile[1];
        PeakList pl = new SimplePeakList("", df);

        Field averageHeight = SimplePeakListRow.class.getDeclaredField("averageHeight");
        averageHeight.setAccessible(true);

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            if ((line = br.readLine()) != null)
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(" ");
                    id = Integer.parseInt(values[0]);
                    rt = Double.parseDouble(values[1]);
                    mz = Double.parseDouble(values[2]);
                    height = Double.parseDouble(values[3]);
                    SimplePeakListRow row = new SimplePeakListRow(id);
                    row.setAverageMZ(mz);
                    row.setAverageRT(rt);
                    averageHeight.set(row, height);
                    pl.addRow(row);
                }
        }

        return pl;
    }

    private static void _main(String[] args) throws Exception {

        new ParentShutdownChecker().start();
        WrapperBase.setupMzMine2Core();

        if (args.length != 3)
            throw new IllegalArgumentException("need 3 parameters: in_file, out_path and config_path");

        String inPath = args[0];
        String outPath = args[1];
        String config_path = args[2];

        System.out.println("json file\n" + config_path);

        String jsonString = new String(Files.readAllBytes(Paths.get(config_path)));

        JSONObject json = new JSONObject(jsonString);
        JSONArray rtToleranceArray = json.getJSONArray("rt_tolerance");
        Boolean rtToleranceIsAbsolute = rtToleranceArray.getBoolean(0);
        Double rtToleranceValue = rtToleranceArray.getDouble(1);

        JSONArray mzToleranceArray = json.getJSONArray("mz_tolerance");
        Double mzTolerancerAbs = mzToleranceArray.getDouble(0);
        Double mzTolerancerPpm = mzToleranceArray.getDouble(1);

        Double maxAdductHeight = json.getDouble("max_adduct_height");

        JSONArray adductsArray = json.getJSONArray("adducts");

        AdductType[] adducts = new AdductType[adductsArray.length()];

        for (int i = 0; i < adductsArray.length(); ++i) {
            String adductId = adductsArray.getJSONArray(i).getString(0);
            Double massShift = adductsArray.getJSONArray(i).getDouble(1);
            adducts[i] = new AdductType(adductId, massShift);
        }

        AdductSearchParameters p = new AdductSearchParameters();

        // p.PEAK_LISTS.setValue();
        p.RT_TOLERANCE.setValue(new RTTolerance(rtToleranceIsAbsolute, rtToleranceValue));
        p.MZ_TOLERANCE.setValue(new MZTolerance(mzTolerancerAbs, mzTolerancerPpm));
        p.MAX_ADDUCT_HEIGHT.setValue(maxAdductHeight);
        p.ADDUCTS.setValue(adducts);

        PeakList pl = readPeakList(inPath);

        AdductSearchTask task = new AdductSearchTask(p, pl);
        task.run();

        PrintWriter writer = new PrintWriter(outPath, "UTF-8");
        System.out.println("write output file to " + outPath);

        try {
            for (PeakListRow row : pl.getRows())
                for (PeakIdentity id : row.getPeakIdentities()) writer.println(row.getID() + "," + id);
        } finally {
            writer.close();
        }
    }

    public static void main(String[] args) {
        try {
            _main(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("!!!ERROR");
            return;
        }
        System.out.println("!!!DONE");
        System.exit(0);
    }
}
