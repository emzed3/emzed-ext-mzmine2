/*
 * RemoveShoulderPeaks.java
 * Copyright (C) 2019 Uwe Schitt <uwe.schmitt@id.ethz.ch>
 *
 * Distributed under terms of the MIT license.
 */

package ch.ethz.id.sis.emzed;

import com.google.common.collect.Range;
import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.*;
import net.sf.mzmine.datamodel.Feature;
import net.sf.mzmine.datamodel.PeakList;
import net.sf.mzmine.datamodel.PeakListRow;
import net.sf.mzmine.datamodel.RawDataFile;
import net.sf.mzmine.modules.impl.MZmineProcessingStepImpl;
import net.sf.mzmine.modules.masslistmethods.ADAPchromatogrambuilder.ADAPChromatogramBuilderParameters;
import net.sf.mzmine.modules.masslistmethods.ADAPchromatogrambuilder.ADAPChromatogramBuilderTask;
import net.sf.mzmine.modules.masslistmethods.shoulderpeaksfilter.PeakModelType;
import net.sf.mzmine.modules.masslistmethods.shoulderpeaksfilter.ShoulderPeaksFilterParameters;
import net.sf.mzmine.modules.masslistmethods.shoulderpeaksfilter.ShoulderPeaksFilterTask;
import net.sf.mzmine.modules.peaklistmethods.peakpicking.deconvolution.ADAPpeakpicking.*;
import net.sf.mzmine.modules.peaklistmethods.peakpicking.deconvolution.DeconvolutionParameters;
import net.sf.mzmine.modules.peaklistmethods.peakpicking.deconvolution.DeconvolutionTask;
import net.sf.mzmine.modules.peaklistmethods.peakpicking.deconvolution.PeakResolver;
import net.sf.mzmine.modules.peaklistmethods.peakpicking.deconvolution.baseline.*;
import net.sf.mzmine.modules.peaklistmethods.peakpicking.deconvolution.centwave.*;
import net.sf.mzmine.modules.peaklistmethods.peakpicking.deconvolution.minimumsearch.*;
import net.sf.mzmine.modules.peaklistmethods.peakpicking.deconvolution.noiseamplitude.*;
import net.sf.mzmine.modules.peaklistmethods.peakpicking.deconvolution.savitzkygolay.*;
import net.sf.mzmine.parameters.parametertypes.tolerances.MZTolerance;
import net.sf.mzmine.project.impl.MZmineProjectImpl;
import net.sf.mzmine.project.impl.RawDataFileImpl;
import net.sf.mzmine.taskcontrol.*;
import net.sf.mzmine.util.R.*;
import net.sf.mzmine.util.maths.CenterFunction;
import net.sf.mzmine.util.maths.CenterMeasure;
import org.json.*;

public class PickPeaks {

    private String inPath;
    private String outPath;
    private Integer minimumScanSpan;
    private Double mzTolerancerAbs;
    private Double mzTolerancerPpm;
    private Double intensityThresh2;
    private Double startIntensity;
    private boolean verbose;

    private Double resolution;
    private PeakModelType peakModel;
    private DeconvolutionParameters deconvolutionParameters;

    private void parseArgs(String[] args) throws Exception {
        // check and get command line parameters
        if (args.length != 3)
            throw new IllegalArgumentException("need 3 parameters: in_file, out_path and config_path");

        inPath = args[0];
        outPath = args[1];
        String config_path = args[2];
        System.out.println("json file\n" + config_path);

        String json = new String(Files.readAllBytes(Paths.get(config_path)));
        System.out.println(json + "\n");

        JSONObject obj = new JSONObject(json);
        verbose = obj.getBoolean("verbose");
        String peakResolverClass = obj.getString("peak_resolver_class");
        JSONObject peakResolverParameters = obj.getJSONObject("peak_resolver_parameters");

        deconvolutionParameters =
            setupDeconvolutionParameters(peakResolverClass, peakResolverParameters, verbose);

        JSONObject acp = obj.getJSONObject("adap_chromatogram_builder");

        minimumScanSpan = acp.getInt("minimum_scan_span");
        JSONArray mzTolerance = acp.getJSONArray("mz_tolerance");
        mzTolerancerAbs = mzTolerance.getDouble(0);
        mzTolerancerPpm = mzTolerance.getDouble(1);
        intensityThresh2 = acp.getDouble("intensity_thresh2");
        startIntensity = acp.getDouble("start_intensity");

        resolution = null;
        peakModel = null;

        if (obj.isNull("rsp_parameters")) {
            return;
        }

        JSONObject rsp_parameters = obj.getJSONObject("rsp_parameters");

        resolution = rsp_parameters.getDouble("resolution");
        String peakModelName = rsp_parameters.getString("peak_model");

        peakModel = null;
        PeakModelType[] peakModels = ShoulderPeaksFilterParameters.peakModel.getChoices();
        for (PeakModelType peakModelChoice : peakModels)
            if (peakModelChoice.name().equals(peakModelName)) {
                peakModel = peakModelChoice;
                return;
            }
        throw new IllegalArgumentException("peak model type " + peakModelName + " unknown.");
    }

    private static DeconvolutionParameters setupDeconvolutionParameters(
        String peakResolverClass, JSONObject peakResolverParameters, boolean verbose)
    throws Exception {

        JSONObject jso = peakResolverParameters;

        DeconvolutionParameters result = null;
        if (peakResolverClass.equals("BaselinePeakDetector"))
            result = setupBaselinePeakDetector(jso, verbose);
        else if (peakResolverClass.equals("NoiseAmplitudePeakDetector"))
            result = setupNoiseAmplitudePeakDetector(jso, verbose);
        else if (peakResolverClass.equals("SavitzkyGolayPeakDetector"))
            result = setupSavitzkyGolayPeakDetector(jso, verbose);
        else if (peakResolverClass.equals("MinimumSearchPeakDetector"))
            result = setupMinimumSearchPeakDetector(jso, verbose);
        else if (peakResolverClass.equals("CentWaveDetector"))
            result = setupCentWaveDetector(jso, verbose);
        else if (peakResolverClass.equals("ADAPDetector")) result = setupADAPDetector(jso,
                    verbose);
        else throw new UnsupportedOperationException("unknown peak resolver " +
                    peakResolverClass);

        result.SUFFIX.setValue("");
        result.AUTO_REMOVE.setValue(false);
        result.RetentionTimeMSMS.setValue(false);

        return result;
    }

    private static DeconvolutionParameters setupBaselinePeakDetector(JSONObject jso,
            boolean verbose)
    throws Exception {
        DeconvolutionParameters result = new DeconvolutionParameters();
        Double minPeakHeight = jso.getDouble("min_peak_height");
        JSONArray peakDuration = jso.getJSONArray("peak_duration");
        Double baselineLevel = jso.getDouble("baseline_level");

        BaselinePeakDetectorParameters p = new BaselinePeakDetectorParameters();
        p.MIN_PEAK_HEIGHT.setValue(minPeakHeight);

        // seconds to minutes:
        p.PEAK_DURATION.setValue(
            Range.closed(peakDuration.getDouble(0) / 60.0, peakDuration.getDouble(1) / 60.0));
        p.BASELINE_LEVEL.setValue(baselineLevel);

        result.PEAK_RESOLVER.setValue(
            new MZmineProcessingStepImpl<PeakResolver>(new BaselinePeakDetector(), p));

        // no logger modification here

        return result;
    }

    private static DeconvolutionParameters setupNoiseAmplitudePeakDetector(
        JSONObject jso, boolean verbose) throws Exception {
        DeconvolutionParameters result = new DeconvolutionParameters();
        Double minPeakHeight = jso.getDouble("min_peak_height");
        JSONArray peakDuration = jso.getJSONArray("peak_duration");
        Double noiseAmplitude = jso.getDouble("noise_amplitude");

        NoiseAmplitudePeakDetectorParameters p = new NoiseAmplitudePeakDetectorParameters();
        p.MIN_PEAK_HEIGHT.setValue(minPeakHeight);

        // seconds to minutes:
        p.PEAK_DURATION.setValue(
            Range.closed(peakDuration.getDouble(0) / 60.0, peakDuration.getDouble(1) / 60.0));
        p.NOISE_AMPLITUDE.setValue(noiseAmplitude);

        result.PEAK_RESOLVER.setValue(
            new MZmineProcessingStepImpl<PeakResolver>(new NoiseAmplitudePeakDetector(), p));

        // no logger modification here

        return result;
    }

    private static DeconvolutionParameters setupSavitzkyGolayPeakDetector(
        JSONObject jso, boolean verbose) throws Exception {
        DeconvolutionParameters result = new DeconvolutionParameters();
        Double minPeakHeight = jso.getDouble("min_peak_height");
        JSONArray peakDuration = jso.getJSONArray("peak_duration");
        Double derivativeThresholdLevel = jso.getDouble("derivative_threshold_level");

        SavitzkyGolayPeakDetectorParameters p = new SavitzkyGolayPeakDetectorParameters();
        p.MIN_PEAK_HEIGHT.setValue(minPeakHeight);

        // seconds to minutes:
        p.PEAK_DURATION.setValue(
            Range.closed(peakDuration.getDouble(0) / 60.0, peakDuration.getDouble(1) / 60.0));
        p.DERIVATIVE_THRESHOLD_LEVEL.setValue(derivativeThresholdLevel);

        result.PEAK_RESOLVER.setValue(
            new MZmineProcessingStepImpl<PeakResolver>(new SavitzkyGolayPeakDetector(), p));

        // no logger modification here

        return result;
    }

    private static DeconvolutionParameters setupMinimumSearchPeakDetector(
        JSONObject jso, boolean verbose) throws Exception {
        DeconvolutionParameters result = new DeconvolutionParameters();
        Double chromatographicThresholdLeveL = jso.getDouble("chromatographic_threshold_level");
        Double searchRtRange = jso.getDouble("search_rt_range");
        Double minRelativeHeight = jso.getDouble("min_relative_height");
        Double minAbsoluteHeight = jso.getDouble("min_absolute_height");
        Double minRatio = jso.getDouble("min_ratio");
        JSONArray peakDuration = jso.getJSONArray("peak_duration");

        MinimumSearchPeakDetectorParameters p = new MinimumSearchPeakDetectorParameters();
        p.CHROMATOGRAPHIC_THRESHOLD_LEVEL.setValue(chromatographicThresholdLeveL);
        p.SEARCH_RT_RANGE.setValue(searchRtRange);
        p.MIN_RELATIVE_HEIGHT.setValue(minRelativeHeight);
        p.MIN_ABSOLUTE_HEIGHT.setValue(minAbsoluteHeight);
        p.MIN_RATIO.setValue(minRatio);

        // seconds to minutes:
        p.PEAK_DURATION.setValue(
            Range.closed(peakDuration.getDouble(0) / 60.0, peakDuration.getDouble(1) / 60.0));

        result.PEAK_RESOLVER.setValue(
            new MZmineProcessingStepImpl<PeakResolver>(new MinimumSearchPeakDetector(), p));

        return result;
    }

    private static DeconvolutionParameters setupCentWaveDetector(JSONObject jso,
            boolean verbose)
    throws Exception {
        DeconvolutionParameters result = new DeconvolutionParameters();
        JSONArray peakDuration = jso.getJSONArray("peak_duration");
        JSONArray peakScales = jso.getJSONArray("peak_scales");

        Double snThreshold = jso.getDouble("sn_threshold");
        String integrationMethod = jso.getString("integration_method");

        CentWaveDetectorParameters p = new CentWaveDetectorParameters();

        // seconds to minutes:
        p.PEAK_DURATION.setValue(
            Range.closed(peakDuration.getDouble(0) / 60.0, peakDuration.getDouble(1) / 60.0));

        p.PEAK_SCALES.setValue(
            Range.closed(peakScales.getDouble(0) / 60.0, peakScales.getDouble(1) / 60.0));

        p.SN_THRESHOLD.setValue(snThreshold);

        if (integrationMethod.equals("UseSmoothedData"))
            p.INTEGRATION_METHOD.setValue(
                CentWaveDetectorParameters.PeakIntegrationMethod.UseSmoothedData);
        else if (integrationMethod.equals("UseRawData"))
            p.INTEGRATION_METHOD.setValue(
                CentWaveDetectorParameters.PeakIntegrationMethod.UseRawData);
        else
            throw new UnsupportedOperationException("dont now integration method " +
                                                    integrationMethod);

        p.RENGINE_TYPE.setValue(REngineType.RCALLER);

        result.PEAK_RESOLVER.setValue(
            new MZmineProcessingStepImpl<PeakResolver>(new CentWaveDetector(), p));

        if (verbose) addLogHandler(CentWaveDetector.class);

        return result;
    }

    private static DeconvolutionParameters setupADAPDetector(JSONObject jso,
            boolean verbose)
    throws Exception {
        DeconvolutionParameters result = new DeconvolutionParameters();
        JSONArray peakDuration = jso.getJSONArray("peak_duration");
        JSONArray rtForCwtScalesDuration = jso.getJSONArray("rt_for_cwt_scales_duration");
        JSONObject snEstimators = jso.getJSONObject("sn_estimators");
        Double snThreshold = jso.getDouble("sn_threshold");
        Double coefAreaThreshold = jso.getDouble("coef_area_threshold");
        Double minFeatHeight = jso.getDouble("min_feat_height");

        ADAPDetectorParameters p = new ADAPDetectorParameters();

        // seconds to minutes:
        p.PEAK_DURATION.setValue(
            Range.closed(peakDuration.getDouble(0) / 60.0, peakDuration.getDouble(1) / 60.0));

        p.RT_FOR_CWT_SCALES_DURATION.setValue(
            Range.closed(
                rtForCwtScalesDuration.getDouble(0) / 60.0,
                rtForCwtScalesDuration.getDouble(1) / 60.0));

        if (snEstimators.length() == 0)
            p.SN_ESTIMATORS.setValue(
                new MZmineProcessingStepImpl<SNEstimatorChoice>(
                    new IntensityWindowsSNEstimator(), new IntensityWindowsSNParameters()));
        else {
            Double halfWaveletWindow = snEstimators.getDouble("half_wavelet_window");
            Boolean absWavCoeffs = snEstimators.getBoolean("abs_wav_coeffs");
            WaveletCoefficientsSNParameters wp = new WaveletCoefficientsSNParameters();
            wp.HALF_WAVELET_WINDOW.setValue(halfWaveletWindow);
            wp.ABS_WAV_COEFFS.setValue(absWavCoeffs);
            p.SN_ESTIMATORS.setValue(
                new MZmineProcessingStepImpl<SNEstimatorChoice>(
                    new WaveletCoefficientsSNEstimator(), wp));
        }

        p.SN_THRESHOLD.setValue(snThreshold);
        p.COEF_AREA_THRESHOLD.setValue(coefAreaThreshold);
        p.MIN_FEAT_HEIGHT.setValue(minFeatHeight);

        result.PEAK_RESOLVER.setValue(
            new MZmineProcessingStepImpl<PeakResolver>(new ADAPDetector(), p));

        if (verbose) addLogHandler(ADAPDetector.class);

        return result;
    }

    private ADAPChromatogramBuilderParameters setupParameters(String suffix) {
        // set ShoulderPeaksFilterParameters
        ADAPChromatogramBuilderParameters p = new ADAPChromatogramBuilderParameters();
        p.massList.setValue("_mass_list" + suffix);
        p.suffix.setValue("");

        p.minimumScanSpan.setValue(minimumScanSpan);
        p.mzTolerance.setValue(new MZTolerance(mzTolerancerAbs, mzTolerancerPpm));
        p.IntensityThresh2.setValue(intensityThresh2);
        p.startIntensity.setValue(startIntensity);
        return p;
    }

    private void removeShoulderPeaks(RawDataFileImpl rdf_in) throws Exception {

        ShoulderPeaksFilterParameters p = new ShoulderPeaksFilterParameters();
        p.autoRemove.setValue(true);
        p.massList.setValue("_mass_list");
        p.suffix.setValue("");
        p.resolution.setValue(resolution);
        p.peakModel.setValue(peakModel);

        runTask(new ShoulderPeaksFilterTask(rdf_in, p));
    }

    private static void runTask(AbstractTask t) throws Exception {
        ComputingIndicator indicator = new ComputingIndicator();
        try {
            indicator.start();
            t.run();
        } finally {
            indicator.shutdown();
        }
        indicator.join();

        TaskStatus status = t.getStatus();
        if (status == TaskStatus.CANCELED) throw new InterruptedException(t.getErrorMessage());
        else if (status == TaskStatus.ERROR) throw new RuntimeException(t.getErrorMessage());
    }

    private static void addLogHandler(Class<?> clz)
    throws IllegalAccessException, NoSuchFieldException {
        Logger.getLogger(clz.getName()).setLevel(Level.FINEST);
        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.FINEST);

        Field field = clz.getDeclaredField("LOG");
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        ((Logger) field.get(null)).addHandler(consoleHandler);
    }

    private static void _main(String[] args) throws Exception {

        new ParentShutdownChecker().start();

        PickPeaks pp = new PickPeaks();

        WrapperBase.setupMzMine2Core();

        pp.parseArgs(args);

        RawDataFileImpl rdf = WrapperBase.readInputFile(pp.inPath);

        WrapperBase.createMassList(rdf);
        String suffix = "";

        if (pp.resolution != null) {
            pp.removeShoulderPeaks(rdf);
            suffix = " ";
        }

        MZmineProjectImpl pimpl = new MZmineProjectImpl();

        runTask(new ADAPChromatogramBuilderTask(pimpl, rdf, pp.setupParameters(suffix)));

        if (pp.verbose) addLogHandler(RSessionWrapper.class);

        PeakList pl = pimpl.getPeakLists()[0];
        System.out.println();
        System.out.println("extracted " + pl.getNumberOfRows() + " chromatograms");
        System.out.println();

        // the RSessionWrapper used from CentWaveDetector redirects out and err to
        // the logger but fails to restore the streams, so we have to do this:
        PrintStream out = System.out;
        PrintStream err = System.err;

        System.out.println("run deconvolution");
        try {
            runTask(
                new DeconvolutionTask(
                    pimpl, pl, pp.deconvolutionParameters, new CenterFunction(CenterMeasure.MEDIAN)));
        } finally {
            System.setOut(out);
            System.setErr(err);
        }

        PeakList[] peakLists = pimpl.getPeakLists();

        PrintWriter writer = new PrintWriter(pp.outPath, "UTF-8");
        System.out.println("write output file to " + pp.outPath);

        try {
            writePeaks(writer, rdf, peakLists[0], false, 0);
            writePeaks(writer, rdf, peakLists[1], true, peakLists[0].getNumberOfRows());
        } finally {
            writer.close();
        }

        System.out.println("wrote output file");
    }

    private static void writePeaks(
        PrintWriter writer, RawDataFile rdf, PeakList pl, boolean writeParentPeakId,
        int offset) {

        String parentPeakId;
        for (PeakListRow row : pl.getRows()) {
            Feature f = row.getPeak(rdf);
            int id = row.getID() + offset;
            if (writeParentPeakId) {
                parentPeakId = "" + f.getParentChromatogramRowID();
            } else {
                parentPeakId = "-";
            }
            Range<Double> mzr = f.getRawDataPointsMZRange();
            Double mzmin = mzr.lowerEndpoint();
            Double mzmax = mzr.upperEndpoint();
            Range<Double> rtr = f.getRawDataPointsRTRange();
            Double rtmin = rtr.lowerEndpoint();
            Double rtmax = rtr.upperEndpoint();
            Double area = f.getArea();
            Double height = f.getHeight();
            writer.println(
                id
                + ", "
                + parentPeakId
                + ","
                + f.getMZ()
                + ","
                + f.getRT() * 60
                + ","
                + mzmin
                + ","
                + mzmax
                + ","
                + rtmin * 60
                + ","
                + rtmax * 60
                + ","
                + area
                + ","
                + height);
        }
    }

    public static void main(String[] args) {
        try {
            _main(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("!!!ERROR");
            return;
        }
        System.out.println("!!!DONE");
        System.exit(0);
    }
}
