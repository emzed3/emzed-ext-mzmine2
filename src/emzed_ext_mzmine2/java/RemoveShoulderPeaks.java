/*
 * RemoveShoulderPeaks.java
 * Copyright (C) 2019 Uwe Schitt <uwe.schmitt@id.ethz.ch>
 *
 * Distributed under terms of the MIT license.
 */

package ch.ethz.id.sis.emzed;

import java.io.*;
import java.net.*;
import net.sf.mzmine.datamodel.MassList;
import net.sf.mzmine.datamodel.Scan;
import net.sf.mzmine.datamodel.impl.SimpleScan;
import net.sf.mzmine.modules.masslistmethods.shoulderpeaksfilter.PeakModelType;
import net.sf.mzmine.modules.masslistmethods.shoulderpeaksfilter.ShoulderPeaksFilterParameters;
import net.sf.mzmine.modules.masslistmethods.shoulderpeaksfilter.ShoulderPeaksFilterTask;
import net.sf.mzmine.project.impl.RawDataFileImpl;

public class RemoveShoulderPeaks {
    private String inPath, outPath;
    private Double resolution;
    private PeakModelType peakModel;

    private void parseArgs(String[] args) throws IllegalArgumentException {
        // check and get command line parameters
        if (args.length != 4)
            throw new IllegalArgumentException(
                "need 4 parameters: in_file, resolution, peak_model_type, out_file");

        inPath = args[0];
        resolution = Double.parseDouble(args[1]);
        outPath = args[3];

        String peakModelName = args[2];

        peakModel = null;
        PeakModelType[] peakModels = ShoulderPeaksFilterParameters.peakModel.getChoices();
        for (PeakModelType peakModelChoice : peakModels)
            if (peakModelChoice.name().equals(peakModelName)) {
                peakModel = peakModelChoice;
                return;
            }
        throw new IllegalArgumentException("peak model type " + peakModelName + " unknown.");
    }

    private ShoulderPeaksFilterParameters setupParameters() {
        // set ShoulderPeaksFilterParameters
        ShoulderPeaksFilterParameters p = new ShoulderPeaksFilterParameters();
        p.autoRemove.setValue(true);
        p.massList.setValue("_mass_list");
        p.suffix.setValue("");
        p.resolution.setValue(resolution);
        p.peakModel.setValue(peakModel);
        return p;
    }

    private static void _main(String[] args) throws Exception {
        RemoveShoulderPeaks rmp = new RemoveShoulderPeaks();
        rmp.parseArgs(args);

        WrapperBase.setupMzMine2Core();

        RawDataFileImpl rdf = WrapperBase.readInputFile(rmp.inPath);

        WrapperBase.createMassList(rdf);

        ShoulderPeaksFilterParameters p = rmp.setupParameters();
        new ShoulderPeaksFilterTask(rdf, p).run();

        RawDataFileImpl rdf_out = new RawDataFileImpl(rmp.outPath);

        for (int i : rdf.getScanNumbers(1)) {
            Scan s = rdf.getScan(i);
            MassList ml = s.getMassList("_mass_list ");
            SimpleScan new_scan = new SimpleScan(s);
            new_scan.setDataPoints(ml.getDataPoints());
            rdf_out.addScan(new_scan);
        }

        WrapperBase.writeOutputFile(rdf_out, rmp.outPath);
    }

    public static void main(String[] args) {
        try {
            _main(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("!!!ERROR");
            return;
        }
        System.out.println("!!!DONE");
    }
}
