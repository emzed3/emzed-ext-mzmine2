/*
 * AdductSearch.java
 * Copyright (C) 2020 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
 *
 * Distributed under terms of the MIT license.
 */

package ch.ethz.id.sis.emzed;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import net.sf.mzmine.datamodel.PeakIdentity;
import net.sf.mzmine.datamodel.PeakList;
import net.sf.mzmine.datamodel.PeakListRow;
import net.sf.mzmine.datamodel.RawDataFile;
import net.sf.mzmine.datamodel.impl.SimplePeakList;
import net.sf.mzmine.modules.peaklistmethods.identification.fragmentsearch.*;
import net.sf.mzmine.parameters.parametertypes.tolerances.MZTolerance;
import net.sf.mzmine.parameters.parametertypes.tolerances.RTTolerance;
import org.json.*;

public class FragmentSearch {

    private static PeakList readPeakList(String path, RawDataFile pm)
    throws IOException, IllegalAccessException, NoSuchFieldException {

        Float rt, rtmin, rtmax, height;
        Double mz, mzmin, mzmax;
        Integer id;

        System.out.println(path);

        RawDataFile[] df = new RawDataFile[] {pm};
        PeakList pl = new SimplePeakList("", df);

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            if ((line = br.readLine()) != null)
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(" ");
                    id = Integer.parseInt(values[0]);
                    rt = Float.parseFloat(values[1]);
                    rtmin = Float.parseFloat(values[2]);
                    rtmax = Float.parseFloat(values[3]);
                    mz = Double.parseDouble(values[4]);
                    mzmin = Double.parseDouble(values[5]);
                    mzmax = Double.parseDouble(values[6]);
                    height = Float.parseFloat(values[7]);
                    EmzedPeakListRow row =
                        new EmzedPeakListRow(id, rt, rtmin, rtmax, mz, mzmin, mzmax, height, pm);
                    pl.addRow(row);
                }
        }

        return pl;
    }

    private static void _main(String[] args) throws Exception {

        new ParentShutdownChecker().start();
        WrapperBase.setupMzMine2Core();

        if (args.length != 4)
            throw new IllegalArgumentException(
                "need 4 parameters: in_file, out_path,  config_path and peakmap_path");

        String inPath = args[0];
        String outPath = args[1];
        String config_path = args[2];
        String peakmapPath = args[3];

        System.out.println("json file\n" + config_path);

        String jsonString = new String(Files.readAllBytes(Paths.get(config_path)));

        JSONObject json = new JSONObject(jsonString);

        JSONArray rtToleranceArray = json.getJSONArray("rt_tolerance");
        Boolean rtToleranceIsAbsolute = rtToleranceArray.getBoolean(0);
        Double rtToleranceValue = rtToleranceArray.getDouble(1);

        JSONArray ms2mzToleranceArray = json.getJSONArray("ms2mz_tolerance");
        Double ms2mzTolerancerAbs = ms2mzToleranceArray.getDouble(0);
        Double ms2mzTolerancerPpm = ms2mzToleranceArray.getDouble(1);

        Double maxFragmentHeight = json.getDouble("max_fragment_height");
        Double minMS2peakHeight = json.getDouble("min_ms2peak_height");

        RawDataFile pm = WrapperBase.readInputFile(peakmapPath);
        FragmentSearchParameters p = new FragmentSearchParameters();

        p.rtTolerance.setValue(new RTTolerance(rtToleranceIsAbsolute, rtToleranceValue));
        p.ms2mzTolerance.setValue(new MZTolerance(ms2mzTolerancerAbs, ms2mzTolerancerPpm));
        p.minMS2peakHeight.setValue(minMS2peakHeight);
        p.maxFragmentHeight.setValue(maxFragmentHeight);

        PeakList pl = readPeakList(inPath, pm);

        FragmentSearchTask task = new FragmentSearchTask(p, pl);
        task.run();

        PrintWriter writer = new PrintWriter(outPath, "UTF-8");
        System.out.println("write output file to " + outPath);

        try {
            for (PeakListRow row : pl.getRows())
                for (PeakIdentity id : row.getPeakIdentities()) writer.println(row.getID() + "," + id);
        } finally {
            writer.close();
        }
    }

    public static void main(String[] args) {
        try {
            _main(args);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("!!!ERROR");
            return;
        }
        System.out.println("!!!DONE");
        System.exit(0);
    }
}
