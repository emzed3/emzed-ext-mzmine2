package ch.ethz.id.sis.emzed;

import java.io.*;
import java.lang.reflect.Field;
import net.sf.mzmine.desktop.impl.HeadLessDesktop;
import net.sf.mzmine.main.impl.MZmineConfigurationImpl;
import net.sf.mzmine.modules.impl.MZmineProcessingStepImpl;
import net.sf.mzmine.modules.rawdatamethods.peakpicking.massdetection.MassDetectionParameters;
import net.sf.mzmine.modules.rawdatamethods.peakpicking.massdetection.MassDetectionTask;
import net.sf.mzmine.modules.rawdatamethods.peakpicking.massdetection.MassDetector;
import net.sf.mzmine.modules.rawdatamethods.peakpicking.massdetection.centroid.CentroidMassDetector;
import net.sf.mzmine.modules.rawdatamethods.peakpicking.massdetection.centroid.CentroidMassDetectorParameters;
import net.sf.mzmine.modules.rawdatamethods.rawdataexport.RawDataExportTask;
import net.sf.mzmine.modules.rawdatamethods.rawdataimport.fileformats.MzMLReadTask;
import net.sf.mzmine.project.impl.MZmineProjectImpl;
import net.sf.mzmine.project.impl.ProjectManagerImpl;
import net.sf.mzmine.project.impl.RawDataFileImpl;

public class WrapperBase {

    public static void setupMzMine2Core() throws Exception {
        // setup mzmine2
        Class<?> clz = Class.forName("net.sf.mzmine.main.MZmineCore");
        Field field = clz.getDeclaredField("projectManager");

        field.setAccessible(true);
        ProjectManagerImpl impl = new ProjectManagerImpl();
        impl.initModule();
        field.set(null, impl);

        field = clz.getDeclaredField("configuration");
        field.setAccessible(true);
        field.set(null, new MZmineConfigurationImpl());

        field = clz.getDeclaredField("desktop");
        field.setAccessible(true);
        field.set(null, new HeadLessDesktop());
    }

    public static RawDataFileImpl readInputFile(String path) throws IOException {
        // read input file
        RawDataFileImpl rdf = new RawDataFileImpl("");

        File mzmlFile = new File(path);
        MZmineProjectImpl pimpl = new MZmineProjectImpl();
        new MzMLReadTask(pimpl, mzmlFile, rdf).run();
        return rdf;
    }

    public static void writeOutputFile(RawDataFileImpl rdf, String path) {

        File out = new File(path);
        RawDataExportTask export = new RawDataExportTask(rdf, out);
        export.run();
    }

    public static void createMassList(RawDataFileImpl rdf) {
        // create masslist by accepting all peaks from scan
        CentroidMassDetector m = new CentroidMassDetector();

        CentroidMassDetectorParameters params = new CentroidMassDetectorParameters();
        params.noiseLevel.setValue(0.0);

        MZmineProcessingStepImpl<MassDetector> step = new MZmineProcessingStepImpl(m, params);

        MassDetectionParameters mdp = new MassDetectionParameters();

        mdp.massDetector.setValue(step);
        mdp.outFilenameOption.setValue(false);
        mdp.name.setValue("_mass_list");

        // run massdetection
        MassDetectionTask mdt = new MassDetectionTask(rdf, mdp);
        mdt.run();
    }

    public static void setupShutdownThreads() {
        Thread t = new ParentShutdownChecker();
        t.start();
    }
}
