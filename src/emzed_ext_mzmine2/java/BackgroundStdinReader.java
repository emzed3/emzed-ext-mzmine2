package ch.ethz.id.sis.emzed;

import java.time.*;
import java.util.*;

class BackgroundStdinReader extends Thread {

    private final long sleep_span_in_ms;
    private String latest;
    private Scanner scanner;

    BackgroundStdinReader(long _sleep_span_in_ms) {
        sleep_span_in_ms = _sleep_span_in_ms;
        scanner = new Scanner(System.in);
        latest = scanner.next();
        System.out.println("got first alive token" + latest);
    }

    public final String getLatest() {
        return latest;
    }

    public void run() {
        try {
            System.out.println("wait for first alive token");
            System.out.flush();
            latest = scanner.next();
            System.out.println("got " + latest);
            System.out.flush();

            while (true) {
                latest = scanner.next();
                Thread.sleep(sleep_span_in_ms);
            }

        } catch (Exception e) {
            // Throwing an exception
            System.out.println("Exception is caught");
        }
    }
}
