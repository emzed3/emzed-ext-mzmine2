package ch.ethz.id.sis.emzed;

import java.sql.Timestamp;
import java.util.*;

class ComputingIndicator extends Thread {

    private boolean running;

    ComputingIndicator() {
        running = true;
    }

    public void shutdown() {
        running = false;
    }

    public void run() {
        Timestamp timestamp;
        int i = 0;

        try {
            while (running) {
                Thread.sleep(100); // 100 ms
                i += 1;
                if (i % 600 == 599) {
                    // once per minute
                    timestamp = new Timestamp(System.currentTimeMillis());
                    System.out.println(timestamp.toInstant() + ": still running");
                    System.out.flush();
                    i = 0;
                }
            }
            timestamp = new Timestamp(System.currentTimeMillis());
            System.out.println(timestamp.toInstant() + ": done");
            System.out.flush();

        } catch (Exception e) {
            // Throwing an exception
            System.out.println("Exception is caught");
        }
    }
}
