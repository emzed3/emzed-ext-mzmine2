package ch.ethz.id.sis.emzed;

import java.util.*;

class ParentShutdownChecker extends Thread {

    /* shuts down java vm in case parent python process dies.
     * this is needed on windows when users shut down python because the java program
     * takes too long. without this mechanisms the java program would continue
     * processing and eat resources, especially when the users starts another jvm.
     *
     * as java has no propper access to the os processes, we implement the following
     * approach:
     *
     *      1. python sends timestamp every 500 ms
     *      2. we read timetsamp from stdin every second
     *      3. if this hangs we call System.exit(42)
     *
     * to detect "haning" in 2. we must reading from stdin in a separate thead
     * BackgroundStdinReader.
     *
     */
    public void run() {

        BackgroundStdinReader t = new BackgroundStdinReader(1000);
        t.start();

        String latest_seen = t.getLatest();
        String latest_reported = latest_seen;
        int misses = 0;

        try {
            while (misses <= 60) {
                Thread.sleep(1000); // msec
                if (latest_reported == t.getLatest()) {
                    misses += 1;
                    System.out.println("misses = " + misses);
                    System.out.flush();
                } else {
                    misses = 0;
                }
                latest_reported = t.getLatest();
                // System.out.println("got " + latest_reported);
            }
            System.out.println();
            System.out.println("parent checker inits shutdown");
            System.out.flush();
            System.exit(42);

        } catch (Exception e) {
            // Throwing an exception
            System.out.println("Exception is caught");
        }
    }
}
