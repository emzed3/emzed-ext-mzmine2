.. emzed documentation master file, created by
   sphinx-quickstart on Fri Aug  2 11:02:04 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to emzed.ext.mzmine2's documentation!
=============================================

*emzed.ext.mzmine* is an extension to *emzed* wrapping some algorithms from 
`mzmine2 <http://mzmine.github.io>`_.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   emzed_ext_mzmine2





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
