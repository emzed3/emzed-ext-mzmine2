from emzed.ext.mzmine2._installers import get_mzmine2_home

import json
import os
import glob
import zipfile

jars = glob.glob(os.path.join(get_mzmine2_home(), "lib", "mzmine*.jar"))
print(jars)
assert len(jars) == 1
jar = jars[0]

zf = zipfile.ZipFile(jar)

entries = {}

for entry in zf.filelist:
    name = entry.filename
    if name.endswith(".class"):
        prefix = name.rsplit(".", 1)[0]
        if "$" in prefix:
            continue

        __, class_name = prefix.rsplit("/", 1)
        path = prefix.replace("/", ".")
        entries[class_name] = path


with open("mzmine2_classes.json", "w") as fh:
    json.dump(entries, fh, indent=4)
