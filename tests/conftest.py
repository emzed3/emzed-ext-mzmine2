#! /usr/bin/env python
# Copyright © 2019 Uwe Schitt <uwe.schmitt@id.ethz.ch>

import os
from functools import partial

import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--runslow", action="store_true", default=False, help="run slow tests"
    )


def pytest_collection_modifyitems(config, items):
    if config.getoption("--runslow"):
        # --runslow given in cli: do not skip slow tests
        return
    skip_slow = pytest.mark.skip(reason="need --runslow option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)


def pytest_itemcollected(item):
    """ we just collected a test item. """
    if any("slow" in name for name in item.fixturenames):
        item.add_marker("slow")


@pytest.fixture
def data_path():
    here = os.path.dirname(os.path.abspath(__file__))
    yield partial(os.path.join, here, "data")
